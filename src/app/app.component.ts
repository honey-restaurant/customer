import { Component } from '@angular/core';

@Component({
  selector: 'bliz-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'customer';
}
