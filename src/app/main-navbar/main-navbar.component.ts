import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';

@Component({
  selector: 'bliz-main-navbar',
  templateUrl: './main-navbar.component.html',
  styleUrls: ['./main-navbar.component.scss'],
})
export class MainNavbarComponent implements OnInit {
  appName: string;

  locationControl = new FormControl();
  searchControl = new FormControl();
  locations: string[] = ['Adambakkam', 'Adayar', 'Meenambakkam'];
  results: string[] = ['Chick Punch', 'Kadai Chicken', 'Chiken Biriyani'];
  filteredLocations: Observable<string[]>;
  searchResults: Observable<string[]>;

  constructor() {}

  ngOnInit(): void {
    // Logo
    this.appName = 'Mablis';

    // Location Search
    this.filteredLocations = this.locationControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filterLocation(value))
    );

    // Restaurant/Dishes/Categories Search
    this.searchResults = this.searchControl.valueChanges.pipe(
      startWith(''),
      map(value => this._filterResults(value))
    );
  }

  private _filterLocation(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.locations.filter(
      option => option.toLowerCase().indexOf(filterValue) === 0
    );
  }

  private _filterResults(value: string): string[] {
    const filterValue = value.toLowerCase();

    return this.results.filter(
      option => option.toLowerCase().indexOf(filterValue) === 0
    );
  }
}
