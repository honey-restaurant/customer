import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { SharedMaterialModule } from './shared/shared-material/shared-material.module';

import { AppComponent } from './app.component';
import { MainNavbarComponent } from './main-navbar/main-navbar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PromotionsComponent } from './promotions/promotions.component';
import { HomeComponent } from './home/home.component';
import { BannerComponent } from './home/banner/banner.component';

@NgModule({
  declarations: [AppComponent, MainNavbarComponent, PromotionsComponent, HomeComponent, BannerComponent],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedMaterialModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
