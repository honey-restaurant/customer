import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatBadgeModule } from '@angular/material/badge';
import { MatToolbarModule } from '@angular/material/toolbar';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    MatBadgeModule,
    MatToolbarModule
  ],
  exports: [
    MatBadgeModule,
    MatToolbarModule
  ]
})
export class SharedMaterialModule { }
